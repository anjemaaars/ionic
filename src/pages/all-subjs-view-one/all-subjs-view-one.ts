import { Component } from '@angular/core';
import { NavController, NavParams, FabContainer } from 'ionic-angular';

import { CurrentSubjsPage } from '../current-subjs/current-subjs';

/*
  Generated class for the AllSubjsViewOne page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-all-subjs-view-one',
  templateUrl: 'all-subjs-view-one.html'
})
export class AllSubjsViewOnePage {
  subject_course: any;
  student_grades: any;
  subj_count: number;
  units_count: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.subj_count = 0;
    this.units_count = 0;
    this.subject_course = this.navParams.get('selected');
    this.student_grades = this.navParams.get('gradelist');

    for (var index = 0; index < this.student_grades.length; index++) {
      this.subj_count++;
      this.units_count = this.units_count + this.student_grades[index].subj_units;
    }
    
  }
  public setGradesOptions(selected_option, fab: FabContainer) {
    fab.close();
    if (selected_option == 'display_current_subjects') {
      this.navCtrl.setRoot(CurrentSubjsPage);
    }
  }
}
