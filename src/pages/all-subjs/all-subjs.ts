import { Component } from '@angular/core';
import { NavController, NavParams, FabContainer, Platform } from 'ionic-angular';

import { CurrentSubjsPage } from '../current-subjs/current-subjs';
import { AllSubjsViewOnePage } from '../all-subjs-view-one/all-subjs-view-one';

/*
  Generated class for the AllSubjs page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-all-subjs',
  templateUrl: 'all-subjs.html'
})
export class AllSubjsPage {
  // Dummy Object array
  student_course_details: Array<{
    course_code: string,
    course_desc: string,
    semester: string,
    school_year: string,
    admission_status: string,
    gen_average: number,
    grades_arry_id: number
  }>;
  student_grades: Array<{
    course_details_id: number,
    subj_code: string,
    subj_desc: string,
    subj_instructor: string,
    subj_units: number,
    subj_schedule: string,
    section_code: string,
    fg_grade: any, // First Grading Grade
    md_grade: any, // Midterms Grade
    fin_grade: any, // Finals Grade
    grade_status: string, // P or F (Passed of Failed)
  }>;
  subj_count: number;
  units_count: number;
  temp: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public plt: Platform) {
    this.units_count = 0;
    this.subj_count = 0;

    this.student_grades = [
      { course_details_id: 1, subj_code: 'CERTSD1', subj_desc: 'Certification Exam Review - SD', subj_instructor: 'Halover, Dr. Ellen', subj_units: 3, subj_schedule:'S 12:00-17:00 F215', section_code: 'IDI', fg_grade: 99, md_grade: 99, fin_grade: 99, grade_status: 'P' },
      { course_details_id: 1, subj_code: 'ENGLIS5', subj_desc: 'Professional English', subj_instructor: 'Cortel, Andrea', subj_units: 3, subj_schedule:'MWF 17:00-18:00 F203', section_code: 'IDI', fg_grade: 86, md_grade: 94, fin_grade: 94, grade_status: 'P' },
      { course_details_id: 1, subj_code: 'IRMGMT1', subj_desc: 'Information Resource Management', subj_instructor: 'Martinez, Erna Kristi', subj_schedule:'MWF 16:00-17:00 F204', subj_units: 3, section_code: 'IDI', fg_grade: 88, md_grade: 90, fin_grade: 93, grade_status: 'P' },
      { course_details_id: 1, subj_code: 'PRSTDY2', subj_desc: 'Project Study II', subj_instructor: 'Carpiso, Cherry Ann', subj_units: 3, subj_schedule:'MWF 11:00-12:00 H208', section_code: 'IDK', fg_grade: 'INC', md_grade: 'INC', fin_grade: 89, grade_status: 'P' },
      { course_details_id: 1, subj_code: 'PESLFC2', subj_desc: 'Bowling', subj_instructor: 'Acob, Candelario', subj_units: 2, subj_schedule: 'TH 14:00-16:00 puyat', section_code: 'GBH', fg_grade: 89, md_grade: 90, fin_grade: 93, grade_status: 'P' }
    ];

    this.student_course_details = [
      { course_code: 'BSIT(ICT)', course_desc: 'Bachelor of Science in Information Technology', semester: 'First', school_year: '1516', admission_status: 'Old Student', gen_average: 84.96, grades_arry_id: 1 },
      { course_code: 'BSIT(ICT)', course_desc: 'Bachelor of Science in Information Technology', semester: 'Second', school_year: '1516', admission_status: 'Old Student', gen_average: 84.67, grades_arry_id: 2 },
      { course_code: 'BSIT(ICT)', course_desc: 'Bachelor of Science in Information Technology', semester: 'Summer', school_year: '1617', admission_status: 'Old Student', gen_average: 89.29, grades_arry_id: 3 },
      { course_code: 'BSIT(ICT)', course_desc: 'Bachelor of Science in Information Technology', semester: 'First', school_year: '1617', admission_status: 'Old Student', gen_average: 85.00, grades_arry_id: 4 },
      { course_code: 'BSIT(ICT)', course_desc: 'Bachelor of Science in Information Technology', semester: 'Second', school_year: '1617', admission_status: 'Old Student', gen_average: 89.78, grades_arry_id: 5 }
    ];

    for (var index = 0; index < this.student_grades.length; index++) {
      this.subj_count++;
      this.units_count = this.units_count + this.student_grades[index].subj_units;
    }
  }

  public getSelectedSemester(selected_option) {
    
    for (var index = 0; index < this.student_course_details.length; index++) {
      if (this.student_course_details[index].grades_arry_id == selected_option) {
        this.temp = this.student_course_details[index];
        break;
      }
    }
    this.navCtrl.push(AllSubjsViewOnePage, {selected : this.temp, gradelist : this.student_grades})
  }

  public setGradesOptions(selected_option, fab: FabContainer) {
    fab.close();
    if (selected_option == 'display_current_subjects') {
      this.navCtrl.setRoot(CurrentSubjsPage);
    }
  }

}
