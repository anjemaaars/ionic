import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, MenuController, Events, LoadingController} from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';

import { HomePage } from '../home/home';
import { MyApp } from '../../app/app.component';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  private login_cred: FormGroup;
  public data;
  public app: MyApp;
  registerCredentials = { username: '', password: '' };
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private formBuilder: FormBuilder, public toastCtrl: ToastController,
              public menuCtrl: MenuController, public events: Events, public loadingCtrl: LoadingController) {
    
      this.menuCtrl.enable(false, 'sidemenu');
      this.login_cred = this.formBuilder.group({
        username: [''],
        password: ['']
      });
    
        this.data = {};
        this.data.username = '';
        this.data.response = '';
  }

  setLogin() {
    // this.presentLoading();
    this.events.publish('setLogin', this.data);
    // let response = this.events.publish('getresponse');
    // console.log(response + ' : ');
    //   if (response != null) {
    //       setTimeout(() => {
    //         this.navCtrl.setRoot(HomePage);
    //       });
    //   } else {
    //       let toast = this.toastCtrl.create({
    //         message: 'Login Failed. Credentials did not match',
    //         duration: 3000,
    //         cssClass: "toast-login-failed"
    //       });
    //       toast.present();
    //   }
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Logging in...",
      duration: 2000
    });
    loader.present();
    
    setTimeout(() => {
      this.navCtrl.setRoot(HomePage);
    }, 2000);
  }
}
