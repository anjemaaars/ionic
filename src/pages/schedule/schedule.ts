import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the Schedule page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html'
})
export class SchedulePage {
  schedule: Array<{
    subj_code: string,
    subj_desc: string,
    subj_lec_units: number,
    subj_lab_units: number,
    subj_schedule: string
  }>;

  subj_count_sched: number;
  units_count_sched: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.subj_count_sched = 0;
    this.units_count_sched = 0;
    this.schedule = [
      { subj_code: 'CERTSD1', subj_desc: 'Certification Exam Review - SD', subj_lec_units: 3, subj_lab_units: 0, subj_schedule: 'IDI S 12:00-17:00 F215' },
      { subj_code: 'ENGLIS5', subj_desc: 'Professional English', subj_lec_units: 3, subj_lab_units: 0, subj_schedule: 'IDI MWF 17:00-18:00 F203' },
      { subj_code: 'IRMGMT1', subj_desc: 'Information Resource Management', subj_lec_units: 3, subj_lab_units: 0, subj_schedule: 'IDI MWF 16:00-17:00 F204' },
      { subj_code: 'PRSTDY2', subj_desc: 'Project Study II', subj_lec_units: 3, subj_lab_units: 0, subj_schedule: 'IDK MWF 11:00-12:00 H208' },
      { subj_code: 'PESLFC2', subj_desc: 'Bowling', subj_lec_units: 3, subj_lab_units: 0, subj_schedule: 'GBH TH 14:00-16:00 Puyat' },
    ];

    for (var index = 0; index < this.schedule.length; index++) {
      this.subj_count_sched++;
      this.units_count_sched = this.units_count_sched + (this.schedule[index].subj_lec_units + this.schedule[index].subj_lab_units);
    }
  }
}
