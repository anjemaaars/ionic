import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the HomeViewPost page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home-view-post',
  templateUrl: 'home-view-post.html'
})
export class HomeViewPostPage {
  post_details: any;
  post_images: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.post_details = this.navParams.get('postdetails');
    this.post_images = this.navParams.get('imgs').img_list;

    console.log(this.post_images);
  }
}
