import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  // make segment 'COURSE AND EDUCATION' active in view
  profile: string = "course";
  
  profile_details: Array<{
    // COURSE SEGMENT
    fname: string,
    mname: string,
    lname: string,
    id: number,
    course_code: string,
    course_desc: string,
    admission_status: string,
    course_year: string, //1st yr, 2nd yr, 3rd yr, 4th yr

    // EDUCATION SEGMENT
    elem: string,
    elem_yr_graduated: number,
    highschool: string,
    hs_yr_graduate: number,

    // CONTACT SEGMENT
    address_in_baguio: string,
    contact_num: string,
    father_name: string,
    father_contact: string,
    mother_name: string,
    mother_contact: string,
    guardian_name: string,
    guardian_contact: string
  }>;
  showprofile: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.profile_details = [
      {
        fname: 'Angelica', mname: 'Rosimo', lname: 'Marcelino', id: 20135845, course_code: 'BSIT(ICT)', course_desc: 'Bachelor of Science in Information Technology', admission_status: 'Old Student', course_year: 'Fourth Year',
        elem: 'Donya Aurora Elementary School', elem_yr_graduated: 2009, highschool: 'Baguio City National High School Main', hs_yr_graduate: 2013,
        address_in_baguio: 'Tacay Rd Central Guisad Baguio City', contact_num: '09999999999',
        father_name: 'Rogelio Marcelino', father_contact: '09999999999', mother_name: 'Rose Marcelino', mother_contact: '09999999999',
        guardian_name: '', guardian_contact: ''
      }
    ]
    this.showprofile = this.profile_details[0];
  }

}
