import { Component } from '@angular/core';

import { NavController, MenuController, NavParams } from 'ionic-angular';
import { HomeViewPostPage } from '../home-view-post/home-view-post';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public people: any;
  feed: Array<{
    postid: number,
    imglink: string,
    title: string,
    content: string,
    date_posted: string,
  }>;
  feed_images: Array<{
    postid: number,
    img_list: Array<{link: string}>
  }>;
  updates: Array<{
    link: string
  }>;


  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController) {
    this.menuCtrl.enable(true, 'sidemenu');

// DUMMY FEEDS
    this.feed = [
      { postid: 1, imglink: 'http://www.pcc.edu.ph/wp-content/uploads/2016/12/DSC_1946.jpg', title: 'Employee Christmas Party', content: 'The Christmas Party was held at the Pines City Colleges covered court and the masters of the ceremony were Mr. Joel Pascua and Ms. Virginia Palome. Everyone enjoyed in the games and food provided. Raffles were also given to the lucky employees who were picked. Everyone got something as a christmas gift from the exchange gift. Our dear vice presidents Ms. Ma. Regina Prats and Dr. Abigail Bersamin also joined the party. The event may have lasted longer then expected but one thing is for sure, everyone had fun and enjoyed.', date_posted: 'Dec 23, 2016' },
      { postid: 2, imglink: 'http://www.pcc.edu.ph/wp-content/uploads/2016/12/DSC_1585.jpg', title: 'Angels in Green', content: 'Pines City Colleges (PCC) faculty and staff spread cheer to the Save Our School Children Foundation, Inc. (SOSCFI) as they substituted Santa during the holiday season. SOSCFI is a non-stock, non – profit service oriented, non-government organization that have grown and expanded to meet the needs of working and indigent children, as well as low income families, student-scholars and minors. The event was a means of fulfilling Pines City Colleges’ commitment to SOSCFI as its adopted community for three years. PCC employees get together every year to make Christmas more joyous for children who need a little help. The program, which is the brainchild of Extension Coordinator Rowena T. Acacio, was aimed to give love, joy, and hope to children and their families during the season of sharing. Christmas with Children is one of the outreach programs the institution offers every year. The event was hosted by Mr. Joel Pascua and Ms. Virginia Palome. Staged at the school’s Covered Court, PCC and SOSCFI gathered last December 17 and participated in the program dubbed ‘Angels in Green: PCC Christmas with Children 2016.’ Ms. Jayrea Eloan, Extension Coordinator of RT, welcomed the attendees to the event. She also thanked everyone for making time to join the program. The program provided food, toys, and other gifts to the recipients. Each employee was paired with a child from SOSCFI. Children laughed as they played with their angels throughout the event. Wide smiles were evident from the participants as they opened their gifts. SOSCFI delivered a response thanking Pines City Colleges for the generous event. Also, an intermission number was presented to the audience. The employees were fascinated by the performers’ graceful dancing. After the event, the group enjoyed a nutritious meal prepared by the College of Tourism and Hotel Management. To note, Project ANGEL, an acronym which means “Advocacies in Networking and Linkages, Green and Clean Environment, Education and Health, and Livelihood,” is a program which aspires to give service, friendship, and protection to the community.', date_posted: 'Dec 20, 2016' },
      { postid: 3, imglink: 'http://www.pcc.edu.ph/wp-content/uploads/2016/11/DSC_0677.jpg', title: 'PCC Tree Planting 2016', content: 'The Pines City Colleges held a Tree Planting event at the Botanical Garden last November 29, 2016. It was a sunny day, a good day to plant trees. The Event was held in the morning, and it was a half day event, the participants were employees and students of Pines City Colleges. The said event is an annual event for the school to help preserve mother nature and to help decrease the effects of global warming.', date_posted: 'Nov 10, 2016' },
      { postid: 4, imglink: 'http://www.pcc.edu.ph/wp-content/uploads/2016/10/DSC_9621.jpg', title: '47th Foundation - Employee Luncheon', content: 'The 3rd Day for the 47th Foundation Anniversary Celebration was for the Employees, It was for the awarding the employees especially those who have spent years with the school. The said event was started with the doxology initiated by Ms. Michelle Langgoy followed by the opening remarks delivered by Ms. Ma. Teresa Q. Padlan.The Recognition of Service Awardees was given by our dear Vice President Ms. Ma. Regina S. Prats, followed by the speech of the service awardees namely Ms. Richelda C. Florendo(25 years) and Ms. Gladys B. Desalit(30 years).The Senior High School presented and intermission after the speech, followed by the seranade of the respiratory therapy students Ms. Cana M. Laguador and Ms. Jussette Christi L. Datu. The Closing Remarks was delivered by Mr. Jose I. Walitang. The Masters of Ceremony were Ms. Joy S. Sib-aten and Mr. Jayson P. Belino.', date_posted: 'Oct 13, 2016' }
    ]

// DUMMY GALLERIES
    this.feed_images = [
      {
        postid: 1, 
        img_list: [
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/12/DSC_1946.jpg'},
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/12/DSC_1950.jpg'},
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/12/DSC_1951.jpg'}
        ]
      },
      {
        postid: 2,
        img_list: [
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/12/DSC_1635.jpg'},
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/12/DSC_1641.jpg'},
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/12/DSC_1642.jpg'},
        ]
      },
      {
        postid: 3,
        img_list: [
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/11/DSC_0834.jpg'},
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/11/DSC_0823.jpg'},
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/11/DSC_0821.jpg'},
        ]
      },
      {
        postid: 4,
        img_list: [
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/10/DSC_9625.jpg'},
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/10/DSC_9622.jpg'},
          {link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/10/DSC_9630.jpg'},
        ]
      }
    ]

// DUMMY UPDATES
    this.updates = [
      { link: 'http://www.pcc.edu.ph/wp-content/uploads/2017/02/PCC-PT-Feb-2017-Tarp-5x7-ft-1.jpg' },
      { link: 'http://www.pcc.edu.ph/wp-content/uploads/2017/02/PCC-SHS-5x7-ft.jpg' },
      { link: 'http://www.pcc.edu.ph/wp-content/uploads/2017/02/mathalinong-grade-11.jpg' },
      { link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/06/PCC-tarp-2x3-ftedited.jpg' },
      { link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/06/PCC-tarp-2x3-ftedited.jpg' },
      { link: 'http://www.pcc.edu.ph/wp-content/uploads/2016/10/PCC-BAPTI-7x5-ft.jpg' },
      { link: 'http://www.pcc.edu.ph/wp-content/uploads/2017/01/5x7.jpg' },
    ]    
  }

  public viewContent(postid) {
    for (var index = 0; index < this.feed.length; index++) {
      if (this.feed[index].postid == postid) {
        this.navCtrl.push(HomeViewPostPage, { postdetails: this.feed[index], imgs: this.feed_images[index] });
      }
    }
  }
}
