import { Injectable, ViewChild } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { ToastController, Events, Nav } from 'ionic-angular';

@Injectable()
export class Tokenservice {
  error: string;
  user: string;
  @ViewChild(Nav) nav: Nav;

  constructor(public http: Http, public toastCtrl: ToastController, public events: Events) {
    this.events.subscribe('tokenservice', () => {
      this.sendDataToken();
    });
  }

  public sendDataToken() {
    let tokendata = localStorage.getItem('id_token');

    if (tokendata != null) {
      console.log('TOKEN SERVICE : token not empty : ' + tokendata);
      var link = 'https://www.sh.pcceduportal.com/api/test2?jwt_token=' + tokendata;
      var data = JSON.stringify({ jwt_token: tokendata });
        
      this.http.post(link, data, {headers: new Headers({"Bearer": tokendata})})
        .subscribe(data => {
            this.tokenSuccess(data["_body"], tokendata);
        }, error => {
          this.error = error;
        }
      );
    }
    else {
      let toast = this.toastCtrl.create({
        message: 'Token not found. Please Log in Again',
        duration: 3000,
        cssClass: "toast-login-failed"
      });
      toast.present();
    }
  }

  public tokenSuccess(token, cred) {
    console.log('RESPONSE : ' + token);
    // if (token.length == 0) {
    //   localStorage.removeItem('id_token');
    //   this.user = null;

    //   let toast = this.toastCtrl.create({
    //     message: 'Login Failed. Credentials did not match',
    //     duration: 3000,
    //     cssClass: "toast-login-failed"
    //   });
    //   toast.present();
    // }
    // else {
    //   console.log('RESPONSE : ' + token);
      // let parsedtoken = JSON.parse(token).jwt_token;

      // this.error = null;
      // localStorage.setItem('id_token', parsedtoken);
      // // this.user = this.jwtHelper.decodeToken(parsedtoken);
      // // localStorage.setItem('profile', JSON.stringify({username: cred.username, password: this.SHA256(cred.password).toString()}));

      // let toast = this.toastCtrl.create({
      //   message: 'Login Success',
      //   duration: 3000,
      //   cssClass: "toast-login-success"
      // });

      // toast.present();
      // setTimeout(() => {
      //   this.nav.setRoot(HomePage);
      // });
    // } 
  }

}
