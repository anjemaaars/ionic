import { Injectable, Component } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';

/*
  Generated class for the Authservice provider. 

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Component({
  providers: [Authservice]
})
@Injectable()
export class Authservice {

  constructor() { }
  
  public static authenticated() {
    return tokenNotExpired();
  }
  
}
