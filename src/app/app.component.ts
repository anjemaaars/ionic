import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, ToastController, LoadingController} from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { JwtHelper } from 'angular2-jwt';

import { LoginPage } from '../pages/login/login';
import { Tokenservice } from '../providers/tokenservice';
import { HomePage } from '../pages/home/home';
import { CurrentSubjsPage } from '../pages/current-subjs/current-subjs';
import { CalendarPage } from '../pages/calendar/calendar';
import { ProfilePage } from '../pages/profile/profile';
import { SchedulePage } from '../pages/schedule/schedule';
import { AppSettingsPage } from '../pages/app-settings/app-settings';

@Component({
  templateUrl: 'app.html',
  providers: [Tokenservice]
})
  
export class MyApp {
  @ViewChild(Nav) nav: Nav; // equivalence of navController
  public data;
  loading: any;
  rootPage: any = HomePage;
  SHA256 = require('crypto-js/sha256');
  authType: string = "login";
  error: string;  
  jwtHelper: JwtHelper = new JwtHelper();
  user: string;
  pages: Array<{ficon: string, title: string, component: any}>;

  constructor(platform: Platform, public http: Http, public events: Events, public toastCtrl: ToastController, public ts: Tokenservice,
            public loadingCtrl: LoadingController) {
      
      if (localStorage.getItem('id_token') != null) {
        this.ts.sendDataToken();
      }

      // Allow 'login.ts' to access function inside 'app.component.ts'
      // event 'setLogin' as stated below is called in 'login.ts'
      events.subscribe('setLogin', (credentials) => {
        this.login(credentials);
      });

      this.pages = [
      { ficon: 'fa fa-newspaper-o', title: 'School News Feed', component: HomePage },
      { ficon: 'fa fa-calendar', title: 'Academic Calendar', component: CalendarPage },
      { ficon: 'fa fa-book', title: 'My Schedule', component: SchedulePage },
      { ficon: 'fa fa-calculator', title: 'My Grades', component: CurrentSubjsPage },
      { ficon: 'fa fa-user', title: 'My Student Profile', component: ProfilePage },
      ];
      
    }
  
  openPage(page) {
    this.nav.setRoot(page.component);
  }

  public login(credentials) {
      let hashpassword = this.SHA256(credentials.password).toString();
      var link = 'https://www.sh.pcceduportal.com/api/test?username=' + credentials.username + '&password=' + credentials.password;
      var data = JSON.stringify({ username: credentials.username, password: hashpassword }); 
      
      this.http.post(link, data)
        .subscribe(data => {
            this.authcheckSuccess(data["_body"], credentials);
        }, error => {
          this.error = error;
          credentials.response = 0;
        }
      );
  }

  public authcheckSuccess(token, cred) {
    if (token.length == 0 || JSON.parse(token).error != "") {
      localStorage.removeItem('id_token');
      this.user = null;
      
      let toast = this.toastCtrl.create({
        message: 'Login Failed. Credentials did not match',
        duration: 3000,
        cssClass: "toast-login-failed"
      });
      toast.present();
    }
    else {
      
      let parsedtoken = JSON.parse(token).jwt_token;
      this.error = null;
      localStorage.setItem('id_token', parsedtoken);
      this.user = this.jwtHelper.decodeToken(parsedtoken);

      let loader = this.loadingCtrl.create({
          content: "Logging in...",
          duration: 2000
      });
      loader.present();
        
        setTimeout(() => {
          this.nav.setRoot(HomePage);
        }, 2000);

    } 
  }

  public logout() {
    localStorage.removeItem('id_token');
    this.user = null;
    this.nav.setRoot(LoginPage);
  }

  public settings() {
    this.nav.setRoot(AppSettingsPage);
  }

}
