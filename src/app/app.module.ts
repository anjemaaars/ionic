import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { AllSubjsPage } from '../pages/all-subjs/all-subjs';
import { CurrentSubjsPage } from '../pages/current-subjs/current-subjs';
import { NgCalendarModule } from 'ionic2-calendar';
import { CalendarPage } from '../pages/calendar/calendar';
import { HomeViewPostPage } from '../pages/home-view-post/home-view-post';
import { AllSubjsViewOnePage } from '../pages/all-subjs-view-one/all-subjs-view-one';
import { ProfilePage } from '../pages/profile/profile';
import { SchedulePage } from '../pages/schedule/schedule';
import { AppSettingsPage } from '../pages/app-settings/app-settings';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    HomeViewPostPage,
    AllSubjsPage,
    AllSubjsViewOnePage,
    CurrentSubjsPage,
    CalendarPage,
    ProfilePage,
    SchedulePage,
    AppSettingsPage
  ],
  imports: [
    NgCalendarModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    HomeViewPostPage,
    AllSubjsPage,
    AllSubjsViewOnePage,
    CurrentSubjsPage,
    CalendarPage,
    ProfilePage,
    SchedulePage,
    AppSettingsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
